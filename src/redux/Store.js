import { compose, createStore, applyMiddleware } from 'redux';
import promiseMiddleware from 'redux-promise-middleware';

import reducers from '../reducers';

const store = createStore(
  reducers,
  undefined,
  compose(
    applyMiddleware(promiseMiddleware()),
  ),
);

export default store;
