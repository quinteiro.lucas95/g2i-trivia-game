import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
} from 'react-native';

import { connect } from 'react-redux';

import { fetchQuestions } from '../../actions/questionsActions';
import styles from './HomeScreen.style';
import Loader from '../Loader';

function mapStateToProps(state) {
    return {
      questions: state.questions.questions,
      isLoading: state.questions.isLoading
    };
};

function mapDispatchToProps(dispatch) {
    return {
        fetchQuestions: () => dispatch(fetchQuestions()),
    }
}

class HomeScreen extends React.Component {

  navigateToQuestion = () => {
    if(!this.props.questions || !this.props.questions.length) {
        this.props.fetchQuestions().then(() => {
          this.props.navigation.navigate({
                      routeName: 'Question',
                      params: {
                          number: 0
                      },
                      key: 'Question' + 0
                  });
        }).catch((err) => {
          console.warn(err);
        });
    } else {
      this.props.navigation.navigate('Question', {number: 0});
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Loader
          loading={this.props.isLoading}
        />
        <View style={styles.titleContainer}>
          <Text style={styles.title}>
            Welcome to the{"\n"}Trivia Challenge!
          </Text>
        </View>
        <View style={styles.titleContainer}>
          <Text style={styles.instructions}>
            You will be presented{"\n"}with 10 True or False{"\n"}questions
          </Text>
        </View>
        <View style={styles.titleContainer}>
          <Text style={styles.instructions}>Can you score 100%?</Text>
        </View>
        <TouchableOpacity
          onPress={this.navigateToQuestion}
          style={styles.buttonContainer}>
          <Text style={styles.beginButton}>BEGIN</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
