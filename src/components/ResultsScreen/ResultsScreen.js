import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';

import { connect } from 'react-redux';
import ResultItem from './ResultItem';

function mapStateToProps(state) {
    return {
      questions: state.questions.questions,
    };
};

import styles from './ResultsScreen.style';

class ResultsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      score: this.calculateScore(),
    }
  }

  navigateHome = () => {
    this.props.navigation.navigate('Home');
  }

  calculateScore = () => {
    const correctAnswers = this.props.questions.filter(question => question.userSuccess);
    return correctAnswers.length;
  }

  render() {
    const score = this.calculateScore();
    return (
      <View style={styles.container}>
        <View style={styles.scoreContainer}>
          <Text style={styles.title}>You scored{"\n"}{this.state.score} / {this.props.questions.length}</Text>
        </View>
        <View style={styles.answersContainer}>
          <FlatList
            overScrollMode="never"
            data={this.props.questions}
            keyExtractor={(item) => item.question}
            renderItem={({item}) => <ResultItem question={item} />}
            />
        </View>
        <TouchableOpacity
          onPress={this.navigateHome}
          style={styles.buttonContainer}>
            <Text style={styles.buttonText}>PLAY AGAIN?</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default connect(mapStateToProps, null)(ResultsScreen);
