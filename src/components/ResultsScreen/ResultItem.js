import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
} from 'react-native';

import styles from './ResultsScreen.style';
import { formatText } from '../../utils/TextUtils';
import { FontAwesome } from '@expo/vector-icons';

export default class ResultItem extends React.PureComponent {
  render() {
    const { question } = this.props;
    return (
      <View
        style={styles.itemContainer}>
        <FontAwesome
          name={question.userSuccess ? 'plus' : 'minus'}
          size={26}
          color="grey"
          />
        <View style={styles.questionContainer}>
          <Text style={styles.question}>{formatText(question.question)}</Text>
        </View>
      </View>
    )
  }
}
