import {
  getQuestions,
} from '../api';

export function fetchQuestions() {
  return {
    type: 'FETCH_QUESTIONS',
    payload: getQuestions(),
  };
}

export function answerQuestion(answer, number) {
  return {
    type: 'ANSWER_QUESTION',
    payload: {
      answer: answer,
      number: number
    }
  }
}
