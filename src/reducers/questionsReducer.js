const initialState = {
  questions: [],
  isLoading: false,
};

function verifyAnswer() {

}

export default function questionsReducer(state = initialState, action) {
  switch (action.type) {
    case 'ANSWER_QUESTION':
      const question = state.questions[action.payload.number];
      const success = (question.correct_answer === action.payload.answer);
      state.questions[action.payload.number].userSuccess = success;
      
      return Object.assign({}, state, {
        isLoading: false,
        questions: state.questions,
      });
    case 'FETCH_QUESTIONS_FULFILLED':
      return Object.assign({}, state, {
        isLoading: false,
        questions: action.payload.data.results
      });
    case 'FETCH_QUESTIONS_PENDING':
      return Object.assign({}, state, {
        isLoading: true,
      });
    case 'FETCH_QUESTIONS_REJECTED':
      return Object.assign({}, state, {
        isLoading: false,
        error: true,
      });
    default:
      return state;
  }
}
