import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 20,
    backgroundColor: '#DFDFDF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scoreContainer: {
    flex: 0.2
  },
  answersContainer: {
    flex: 0.7
  },
  buttonContainer: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 25,
    alignSelf: 'center',
    fontWeight: 'bold',
    color: 'black',
    textAlign: 'center',
  },
  buttonText: {
    fontSize: 25,
    alignSelf: 'center',
    color: 'grey',
  },
  itemContainer: {
    flexDirection: 'row',
    margin: 10,
  },
  questionContainer: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  question: {
    color: 'grey',
    fontSize: 16,
    marginLeft: 10,
  },
});

export default styles;
