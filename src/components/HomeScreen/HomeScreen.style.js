import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#DFDFDF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleContainer: {
    flex: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonContainer: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 25,
    alignSelf: 'center',
    fontWeight: 'bold',
    color: 'black',
    textAlign: 'center',
  },
  instructions: {
    fontSize: 20,
    alignSelf: 'center',
    textAlign: 'center',
  },
  beginButton: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'grey',
    textAlign: 'center',
  },
});

export default styles;
