function formatText(text) {
    let formattedText = text.replace(/&quot;/g, '\"');;
    formattedText = formattedText.replace(/&#039;/g, "'");
    return formattedText;
}

module.exports = {
  formatText,
}
