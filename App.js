import React from 'react';
import { Provider } from 'react-redux';

import Navigation from './src/Navigation';
import store from './src/redux/Store';

export default class App extends React.PureComponent {
  render() {
    return (
      <Provider store={store}>
        <Navigation />
      </Provider>
    );
  }
}
