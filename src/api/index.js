import axios from 'axios';

function getQuestions() {
  const url = 'https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean';
  return axios.get(url);
}

module.exports = {
  getQuestions,
};
