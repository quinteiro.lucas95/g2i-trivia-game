import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    flexDirection: 'column',
    backgroundColor: '#DFDFDF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  categoryContainer: {
    flex: 0.3,
  },
  questionContainer: {
    flex: 0.5,
    borderWidth: 1,
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pageIndicatorContainer: {
    flex: 0.1,
    justifyContent: 'center'
  },
  pageIndicator: {
    fontSize: 14,
  },
  answerContainer: {
    flex: 0.2,
    flexDirection: 'row'
  },
  answer: {
    borderWidth: 1,
    padding: 5,
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
  answerText: {
    fontSize: 14,
    alignSelf: 'center',
    textAlign: 'center',
  },
  category: {
    fontSize: 22,
    alignSelf: 'center',
    fontWeight: 'bold',
    color: 'black',
    textAlign: 'center',
  },
  question: {
    fontSize: 20,
    alignSelf: 'center',
    textAlign: 'center',
  },
});

export default styles;
