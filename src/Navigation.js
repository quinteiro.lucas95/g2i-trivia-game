import { createStackNavigator } from 'react-navigation';

import HomeScreen from './components/HomeScreen/HomeScreen';
import QuestionScreen from './components/QuestionScreen/QuestionScreen';
import ResultsScreen from './components/ResultsScreen/ResultsScreen';

const AppStack = createStackNavigator({
  Home: {
    screen: HomeScreen,
  },
  Question: {
    screen: QuestionScreen,
  },
  Results: {
    screen: ResultsScreen,
  }
},
{
  navigationOptions: () => ({
    header: null,
  }),
});

export default AppStack;
