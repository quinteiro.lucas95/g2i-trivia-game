import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
} from 'react-native';

import { connect } from 'react-redux';

import styles from './QuestionScreen.style';
import { answerQuestion } from '../../actions/questionsActions';
import { formatText } from '../../utils/TextUtils';

function mapStateToProps(state) {
    return {
      questions: state.questions.questions,
      isLoading: state.questions.isLoading
    };
};

function mapDispatchToProps(dispatch) {
    return {
      answerQuestion: (answer, number) => dispatch(answerQuestion(answer, number)),
    }
}


class QuestionScreen extends React.Component {

  constructor(props) {
    super(props);
    const number = this.props.navigation.getParam('number', 0);
    this.state = {
      number: number,
    };
  }
  navigateToResults = () => {
    this.props.navigation.navigate('Results');
  }

  navigateToNextQuestion = () => {
    const nextQuestion = this.state.number + 1;
    this.props.navigation.navigate({
                routeName: 'Question',
                params: {
                    number: nextQuestion
                },
                key: 'Question' + nextQuestion
            });
  }

  answerQuestion = (answer) => {
    this.props.answerQuestion(answer, this.state.number);
    if ((this.state.number + 1) < this.props.questions.length) {
      this.navigateToNextQuestion();
    } else {
      this.navigateToResults();
    }
  }

  render() {
    const question = this.props.questions[this.state.number];
    return (
      <View style={styles.container}>
        <View style={styles.categoryContainer}>
          <Text style={styles.category}>{question.category}</Text>
        </View>
        <View style={styles.questionContainer}>
          <Text style={styles.question}>{formatText(question.question)}</Text>
        </View>
        <View style={styles.pageIndicatorContainer}>
          <Text style={styles.pageIndicator}>{this.state.number + 1}/{this.props.questions.length}</Text>
        </View>
        <View style={styles.answerContainer}>
          <TouchableOpacity
            style={styles.answer}
            onPress={() => {this.answerQuestion('True')}}
          >
            <Text style={styles.answerText}>TRUE </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.answer}
            onPress={() => {this.answerQuestion('False')}}
          >
            <Text style={styles.answerText}>FALSE</Text>
          </TouchableOpacity>
        </View>
      </View>

    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(QuestionScreen);
